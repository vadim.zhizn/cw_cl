import time
from datetime import datetime

from Tomita import mentions
import newsParser as parser
from Word2vec.synonyms import take_synonyms

URL = 'http://www.volgograd.ru/news/'
HOST = 'http://www.volgograd.ru'

while(True):
    print('Парсер запущен...')
    parser.parse(URL, HOST)
    print('Поиск упоминаний...')
    count_ment = mentions.take_mentions()
    print(f'Найдено новых упоминаний - {count_ment}!')
    print('Поиск контекстных синонимов...')
    count_syn = take_synonyms()
    print(f'Найдено новых ключевых слов с контекстными синонимами - {count_syn}!')
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")
    print('Следующий запуск через 1 час')
    print('Текущее время: '+current_time)
    time.sleep(60 * 60)