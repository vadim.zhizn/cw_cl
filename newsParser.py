from time import sleep

import requests
from bs4 import BeautifulSoup
from pymongo import MongoClient


def get_html(url, params=None):
    return requests.get(url, params=params)

def get_news_text(url):
    html = get_html(url)
    if html.status_code != 200:
        return "Error to load"
    soup = BeautifulSoup(html.text, 'html.parser')
    try:
        paragraphs = soup.find('div', class_="news-detail").find_all('p')
    except:
        print(url)
        return "Error to load"
    text = ""
    for paragraph in paragraphs:
        text += paragraph.get_text(strip=True)
    return text

def get_content(html,HOST):
    f = open('./Tomita/Files/input.txt', encoding='utf-8', mode='w')
    soup = BeautifulSoup(html.text, 'html.parser')
    items = soup.find_all('div', class_='col-md-12 news-item')
    client = MongoClient(host='localhost', port=27017)
    db = client['news_parser']
    news_coll = db['news']
    for item in items:
        name = item.find('a').get_text()
        link = HOST + item.find('a').get('href')
        text = get_news_text(link)
        if text == 'Error to load':
            continue
        date = item.find('div', class_='date').get_text()
        news = {'Name':name,
                'Link':link,
                'Text':text,
                'date':date}
        if news_coll.find_one({'Link':link}) is None:
            news_coll.insert_one(news)
            f.write(text)
    f.close()

def parse(URL, HOST):
    html = get_html(URL)
    if html.status_code == 200:
        get_content(html, HOST)
    else:
        print('Error')