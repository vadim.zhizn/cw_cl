# Преднастройка   
### Для работы требуется SDK для VirtualBox. Он находится в папке "Libs"
### В ней найти файл vboxapisetup.py и выполнить команды:   
   
#### python vboxapisetup.py install    
#### python -m pip install virtualbox   
   
# Связывание папок   
### Необходимо связать папки "~/Tomita/Files" и "/home/vagrant/Tomita"   
### Папка tomita-parser должна находиться по пути "/home/vagrant/tomita-parser   
### Либо менять в файле "tomita.py" выполнение команд   

# Модель w2v
### ссылка на полученный датасет .csv в 15к новостей : https://drive.google.com/file/d/11Vl9HAVCoKE1wL8Kn6Hrvd5gZbzFKUzr/view?usp=sharing
