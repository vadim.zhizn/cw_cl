import re
import pymongo
from nltk.corpus import stopwords
from pymorphy2 import MorphAnalyzer

from Word2vec.module_2 import getSimWordsList, getSimWords

def get_sentences(collection):
    text = []
    result = []
    for i in collection.find():
        text.append(i['Mentioning'].strip())
    for i in text:
        splitted_text = i.split(' ')
        for j in splitted_text:
            #if(len(j) > 2):
                result.append(j.strip().lower())
    return result

def set_syn(name_mentionings,name_syn):
    client = pymongo.MongoClient('localhost', 27017)
    list = set()
    count = 0
    db = client['news_parser']
    tmp_collection = db[name_mentionings]
    for i in tmp_collection.find():
        list.add(i['Name'].strip().lower())
    collection = db[name_syn]
    for item in list:
        if collection.find_one({'Keyword': item}) is None:
            try:
                count += 1
                FIO = str.split(item)
                sim_coef = getSimWordsList(FIO, 7)
                row = {'Keyword': item, 'Simword_coef': ' '.join([str(elem) for elem in sim_coef])}
                collection.insert_one(row)
            except KeyError:
                try:
                    FIO = str.split(item)
                    sim_coef = getSimWords(FIO[0], 7)
                    row = {'Keyword': item, 'Simword_coef': ' '.join([str(elem) for elem in sim_coef])}
                    collection.insert_one(row)
                except KeyError:
                    print(FIO[0] + " not found in dictionary!")
                    print(item)
                    count -=1
                    continue
    return count

#patterns = "[A-Za-z0-9!#$%&'()*+,./:;<=>?@[\]^_`{|}~—\"\«»-]+"
#stopwords_ru = stopwords.words("russian")
#morph = MorphAnalyzer()


def take_synonyms():
    return set_syn('mentionings','mentioning_syn')

