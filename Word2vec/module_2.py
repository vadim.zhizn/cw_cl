import pandas as pd
from gensim.models import Word2Vec
model = Word2Vec(
    min_count=1, #10
    window=2,
    #size=300,
    negative=10,
    alpha=0.03,
    min_alpha=0.0007,
    sample=6e-5,
    sg=1)

#получаем общие контекстные синонимы для нескольки слов
def getSimWordsList(list,top=10):
  return model.wv.most_similar(positive=list,topn=top)

#получаем общие контекстные синонимы для единственного слова
def getSimWords(word,top=10):
    #Вывод похожих слов
    return model.wv.similar_by_word(word,topn=top)

model = Word2Vec.load(".\Word2vec\w2v_model_vlg2")
