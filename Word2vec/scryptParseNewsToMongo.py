from time import sleep
import random
import requests
from bs4 import BeautifulSoup
import csv

from pymongo import MongoClient

URL = 'http://www.volgograd.ru/news/'
HOST = 'http://www.volgograd.ru'

PARAMS = {}
data = []


def get_html(url, params):
    return requests.get(url, params=params)


def get_news_text(url):
    html = get_html(url, PARAMS)
    if html.status_code != 200:
        return "Error to load"
    soup = BeautifulSoup(html.text, 'html.parser')
    try:
        paragraphs = soup.find('div', class_="news-detail").find_all('p')
    except AttributeError:
        return "err"
    text = ""
    for paragraph in paragraphs:
        text += paragraph.get_text(strip=True)
    return text


def get_content(html):
    soup = BeautifulSoup(html.text, 'html.parser')
    items = soup.find_all('div', class_='col-md-12 news-item')
    client = MongoClient(host='localhost', port=27017)
    db = client['news_parser']
    news_coll = db['news_dataset']
    for item in items:
        name = item.find('a').get_text()
        link = HOST + item.find('a').get('href')
        text = get_news_text(link)
        if text == 'Error to load':
            continue
        date = item.find('div', class_='date').get_text()
        news = {'Name': name,
                'Link': link,
                'Text': text,
                'date': date}
        if news_coll.find_one({'Link': link}) is None:
            news_coll.insert_one(news)


def parse(start, end):
    while start < end:
        PARAMS = {'PAGEN_1': start}
        html = get_html(URL, PARAMS)
        start += 1

        if html.status_code == 200:
            get_content(html)
        else:
            print('Error')


i = 1
j = 760
while i < j:
    parse(i, i + 1)
    print(i)
    i += 1
    sleep(float(random.randint(0, 15)) * random.random())

