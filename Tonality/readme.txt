Для определения тональности предложения нужно запускать TonalitySpecifier.
Прежде чем его запускать нужно запустить TonalityModel для генерации модели.

Для работы TonalityModel нужно установить:

pip install pymorphy2
pip install nltk==3.3
pip install pandas

Для запуска:
python TonalityModel.py

Результатом работы будет файл my_classifier.pickle, которой является готовой
моделью для определения тональности текста.

После этого можно запускать TonalitySpecifier.