import nltk
# nltk.download('stopwords')
# nltk.download('averaged_perceptron_tagger_ru')
# nltk.download('wordnet')
# nltk.download('punkt')
import pymorphy2
from pymorphy2 import MorphAnalyzer
import pandas as pd
import os
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import stopwords
from nltk.tag import pos_tag
from nltk.tokenize import word_tokenize
from nltk import FreqDist, classify, NaiveBayesClassifier
from itertools import chain
import re, string, random
import numpy as np
import csv
import pickle

f = open('./Tonality/my_classifier.pickle', 'rb')
classifier = pickle.load(f)
f.close()

def remove_noise(tweet_tokens, stop_words = ()):

    cleaned_tokens = []

    for token, tag in pos_tag(tweet_tokens, lang="rus"):
        token = re.sub('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+#]|[!*\(\),]|'\
                       '(?:%[0-9a-fA-F][0-9a-fA-F]))+','', token)
        token = re.sub("(@[A-Za-z0-9_]+)","", token)

        if tag.startswith("NN"):
            pos = 'n'
        elif tag.startswith('VB'):
            pos = 'v'
        else:
            pos = 'a'

        lemmatizer = WordNetLemmatizer()
        token = lemmatizer.lemmatize(token, pos)

        if len(token) > 0 and token not in string.punctuation and token.lower() not in stop_words:
            cleaned_tokens.append(token.lower())
    return cleaned_tokens
def get_tonality(custom_tweet):
    custom_tokens = remove_noise(word_tokenize(custom_tweet))
    return (custom_tweet, classifier.classify(dict([token, True] for token in custom_tokens)))
