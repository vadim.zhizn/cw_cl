import virtualbox
from Tomita.gzt_former import form_gzt
def run_tomita():
    vbox = virtualbox.VirtualBox()
    vm = vbox.find_machine('Vagrant_default_1618585220513_50216')
    session = vm.create_session()
    session.console.keyboard.put_keys("cd ~\n")
    session.console.keyboard.put_keys("cp Tomita/input.txt tomita-parser/build/bin\n")
    session.console.keyboard.put_keys("cd tomita-parser/build/bin\n")
    session.console.keyboard.put_keys("./tomita-parser ./config.proto\n")
    session.console.keyboard.put_keys("cp ./mentionings.txt ../../../Tomita/\n")

def update_gzt(form_new_gzt = False):
    if form_new_gzt:
        form_gzt()
    vbox = virtualbox.VirtualBox()
    vm = vbox.find_machine('Vagrant_default_1618585220513_50216')
    session = vm.create_session()
    session.console.keyboard.put_keys("cd ~\n")
    session.console.keyboard.put_keys("cp Tomita/dic.gzt tomita-parser/build/bin\n")
