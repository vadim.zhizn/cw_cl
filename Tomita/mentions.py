import re
from pymongo import  MongoClient
from Tonality import TonalitySpecifier
from Tomita import tomita
def take_mentions():
    tomita.run_tomita()
    f = open('./Tomita/Files/mentionings.txt', encoding='utf-8')
    client = MongoClient(host='localhost', port=27017)
    mentionings_coll = client['news_parser']['mentionings']
    text = f.read()
    f.close()
    pattern = re.compile('[А-Яа-яЁё][^.!?]*[.!?] (\n\tMentioning\n\t{[^}]*})+')
    mentionings = pattern.finditer(text)
    count = 0
    for ment in mentionings:
        ment = ment.group(0)
        sentence = ment[:ment.find('Mentioning')].strip()
        tonality = TonalitySpecifier.get_tonality(sentence)[1]
        if mentionings_coll.find_one({'Sentence': sentence}) is None:
             pattern = re.compile('\tMentioning\n\t{[^}]*}')
             for name in pattern.findall(ment):
                 count+=1
                 name = name[name.find('name = ') + 7:name.find('n\t}')].strip().title()
                 mentionings_coll.insert_one({'Sentence' : sentence, 'Name' : name, 'Tonality' : tonality })
    return count

