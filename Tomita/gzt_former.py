import requests
from bs4 import BeautifulSoup
import re
def form_gzt():

    URL = 'https://avolgograd.com/sights/'
    HOST = 'https://avolgograd.com'
    PARAMS = {'obl': 'vgg'}

    html = requests.get(URL, params=PARAMS,
                        headers = {"User-Agent" : "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36"}).text
    f_gzt = open('./Tomita/Files/dic.gzt', encoding='utf-8', mode='w')
    f_gzt.write('encoding "utf8";\n')
    f_gzt.write('import "base.proto";\n')
    f_gzt.write('import "articles_base.proto";\n')
    f_gzt.write('import "fact_types.proto";\n')
    f_gzt.write('import "kwtypes.proto";\n')
    f_gzt.write('TAuxDicArticle "FormulaExtract"{key = {"tomita:first_grammar.cxx" type=CUSTOM}}\n')

    soup = BeautifulSoup(html, 'html.parser')
    attractions = soup.find_all('div', class_='ta-211')

    for attraction in attractions:
        attractionName = attraction.text
        if attractionName == 'Память':
            attractionName = 'Музей "Память"'

        f_gzt.write('attractions ' + '"' + "_".join([spl for spl in attractionName.lower().split(' ')]) + '"\n{\n\t')
        f_gzt.write('key = \'' + attractionName + '\';\n\t')
        f_gzt.write('lemma = \''+attractionName + '\';\n}\n')

    for i in range(9):
        url = 'https://global-volgograd.ru/person?offset=' + str(20 * i)
        html = requests.get(url)
        soup = BeautifulSoup(html.text, 'html.parser')
        persons = soup.find_all('div', class_='person-block')

        for person in persons:
            full_name = person.find('div', class_='title').text.title().strip()
            names = re.split('\\s+', full_name)

            f_gzt.write('persons "' + '_'.join([spl for spl in full_name.lower().split(' ')])+'"\n')
            f_gzt.write('{\n\tkey="' + names[0] + '" | "' + names[1] + ' ' + names[0] + '" | "' + full_name + '"')
            if len(names) == 3:
                f_gzt.write(' | "' + names[0] + ' ' + names[1][0] + '. ' + names[2][0] + '."')
                lemma = names[0] + ' ' + names[1] + ' ' + names[2] + '";\n}\n'
            else:
                f_gzt.write(' | "' + names[0] + ' ' + names[1][0] + '."')
                lemma = names[0] + ' ' + names[1] + '";\n}\n'
            f_gzt.write(';\n\tlemma="')
            f_gzt.write(lemma)

    f_gzt.close()

